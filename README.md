## [**Setting Up Shell GPT on Linux Terminal**](https://gitlab.com/naveenz123/using-chatgpt-via-the-linux-terminal)

Shell GPT is a command-line productivity tool powered by OpenAI's ChatGPT (GPT-3.5). It can be used to generate shell commands, code snippets, comments, and documentation by building on the capabilities of ChatGPT. This documentation elaborates the installation process for Shell GPT along with issues that were faced during installation and how they were resolved. 

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

The following installations are required:-
1. python3 
`apt install python3`

2. pip3
`apt install python3-pip`

3. python3-venv 
`apt install python3-venv`

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Future Scope
Setting up Shell GPT through a fish shell 

## References 

https://pypi.org/project/shell-gpt/0.6.0/

https://beebom.com/how-use-chatgpt-linux-terminal/


